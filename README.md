# Oauth2 Proxy example

Try Oauth2 Proxy using OIDC with GitLab as IdP

## Arch

![diagram](./images/diagram.dio.png)

## Requirement

- helm v3.12.1
- helmfile v0.155.0

## Usage

1. register Oauth2 client with GitLab
   - reference
     - https://docs.gitlab.com/ee/integration/oauth_provider.html
2. obtain client id and client secret
3. save client id and client secret as helm value file
    ```sh
    cat <<-EOF > ./values/oauth2-proxy.secrets.yaml
    config:
      clientID: "<YOUR_CLIENT_ID>"
      clientSecret: "<YOUR_CLIENT_SECRET>"
    EOF
    ```
4. create helm release
    ```sh
    helmfile apply
    ```
5. access following URL using browser
   - http://localhost:30080
